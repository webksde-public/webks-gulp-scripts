import gulp from 'gulp';
import rsync from 'gulp-rsync';
import plumber from 'gulp-plumber';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import postcss from 'gulp-postcss';
import sassInheritance from 'gulp-sass-inheritance';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import rename from 'gulp-rename';
import stripDebug from 'gulp-strip-debug';
import inquirer from 'inquirer';
import npmDist from 'gulp-npm-dist';
import notify from 'gulp-notify';
import browserSync from 'browser-sync';

import { spawn } from 'child_process';

import { livereload, webserver, folderNames, paths, globs, gulpNpmDistExcludes } from './gulpfile.variables.mjs';

const sass = gulpSass(dartSass);
const sync = gulp.series(syncFiles, reloadBrowser);

// Console colors (see: https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color)
const successColor = '\x1b[32m';
const infoColor = '\x1b[36m';
const warningColor = '\x1b[33m';
const alertColor = '\x1b[33m';

// =============================================================================
// =========================== Devmode =========================================
// =============================================================================

const isDevMode = process.argv.includes('--devmode');

function logDevmodeStatus() {
  if (isDevMode === true) {
    console.log(warningColor, 'Devmode status: ' + isDevMode);
  } else {
    console.log(infoColor, 'Devmode status: ' + isDevMode + ' (add flag --devmode to activate)');
  }
}

// =============================================================================
// Private Tasks
// =============================================================================

// Run commands
const run = function (command) {
  return new Promise((resolve, reject) => {
    const childProcess = spawn(command, { shell: true });

    childProcess.on('error', reject);
    childProcess.on('exit', resolve);
  });
};

// NPM Update
function npmUpdate(done) {
  run('npm update')();
  done();
}

function syncFiles() {
  // Hints:
  // - If you dont use './' as src folder, make sure to write the src paths like so: './folder/'. Otherwise the removal of remote files wont work ("clean"-option").
  //   Furthermore the recusrive option should be set to true, see: https://github.com/jerrysu/gulp-rsync/issues/29
  //   To use all configured "paths" instead, set Object.values(paths).
  // - Hidden files and folders are excluded (.*, .*/)
  // - DONT add since: gulp.lastRun(syncFiles) to this task
  return gulp
    .src('./', { base: './' })
    .pipe(plumber())
    .pipe(
      rsync({
        destination: `${webserver.user}@${webserver.host}:${webserver.remotePath}`,
        exclude: webserver.exclsionGlobs,
        recursive: true,
        clean: true,
        compress: true,
        progress: true,
        incremental: true,
        chmod: 'ugo=rwX',
        ssh: true,
        port: webserver.port,
        identity: webserver.key,
        relative: true,
        emptyDirectories: true,
      }),
    );
}

function reloadBrowser(done) {
  browserSync.reload();
  done();
}

// TODO: this doesnt work well from within WSL2, it seems that the linux-gui-apps take priority here (=> Linux Firefox opens instead Windows FF)
function openBrowser(done) {
  spawn('cmd.exe', ['/C', 'start', livereload.browser, livereload.proxyUrl]);
  done();
}

function moveFrontendLibraries() {
  return gulp
    .src(
      npmDist({
        copyUnminified: true,
        copyFullRepo: true,
        replaceDefaultExcludes: true,
        excludes: gulpNpmDistExcludes,
      }),
      { base: './node_modules' },
    )
    .pipe(
      notify({
        message: 'Move Frontend Libraries: finished!',
        onLast: true,
      }),
    )
    .pipe(gulp.dest(paths.frontendLibraries));
}

function updateAssets() {
  logDevmodeStatus();
  const question = [
    {
      type: 'confirm',
      name: 'upload',
      message: 'Do you want the assets to be uploaded also? (or just copy em local)',
      default: false,
    },
  ];
  return inquirer.prompt(question).then((answer) => {
    if (!answer.upload) {
      // Just local
      console.log(infoColor, 'Starting Update (just local).');
      gulp.series(npmUpdate, moveFrontendLibraries);
    } else {
      // Local & upload
      console.log(infoColor, 'Starting Update (+ uploading files).');
      gulp.series(npmUpdate, moveFrontendLibraries, sync);
    }
  });
}

// =============================================================================
// Public Tasks
// =============================================================================

gulp.task('sass', function () {
  let stream = gulp
    .src(globs.scss, { since: gulp.lastRun('sass') })
    .pipe(plumber())
    .pipe(sassInheritance({ dir: paths.scss })) // Specify the path to your SCSS files directory
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({ suffix: '.min' }));
  if (isDevMode) {
    stream = stream.pipe(sourcemaps.write('.'));
  } else {
    stream = stream.pipe(postcss([ autoprefixer(), cssnano({ zindex: false }) ]));
  }
  return stream.pipe(gulp.dest(paths.css));
});

gulp.task('js', function () {
  let stream = gulp.src(globs.js, { since: gulp.lastRun('js') }).pipe(plumber());
  if (isDevMode) {
    stream = stream.pipe(rename({ suffix: '.min' }));
  } else {
    stream = stream
      .pipe(stripDebug())
      .pipe(uglify())
      .pipe(rename({ suffix: '.min' }));
  }
  return stream.pipe(gulp.dest(paths.jsDist));
});

gulp.task('watch', function () {
  logDevmodeStatus();
  gulp.watch(globs.scss, gulp.series('sass'));
  gulp.watch(globs.js, gulp.series('js'));
});

gulp.task('watchUpload', function () {
  logDevmodeStatus();
  browserSync.init({
    proxy: livereload.url,
    browser: livereload.browser,
  });
  gulp.watch(globs.scss, gulp.series('sass', sync));
  gulp.watch(globs.js, gulp.series('js', sync));
  gulp.watch(globs.templates, sync);
  gulp.watch(globs.misc, sync);
  gulp.watch(globs.images, sync);
  gulp.watch(globs.fonts, sync);
});

gulp.task('build', function (done) {
  // logDevmodeStatus();
  var doBuild = gulp.series(moveFrontendLibraries, 'sass', 'js');
  doBuild();
  console.log(successColor, 'Building done (Moved Frontend Libraries, processed SASS & JS)');
  done();
});

// Define some shortcuts
gulp.task('mfl', function (done) {
  var doMoveFrontendLibraries = gulp.series(moveFrontendLibraries);
  doMoveFrontendLibraries();
  console.log(successColor, 'Moved frontend libraries.');
  done();
});
gulp.task('mflu', function (done) {
  var doMoveAndUploadFrontendLibraries = gulp.series(moveFrontendLibraries, sync);
  doMoveAndUploadFrontendLibraries();
  console.log(successColor, 'Moved (+ uploaded) frontend libraries.');
  done();
});

// Maintenance Tasks
// -- Self Update: Update gulpfile.js from https://www.npmjs.com/package/webks-gulp-scripts
gulp.task('selfUpdate', function (done) {
  // TODO: Store the answered question in the package.json?
  const question = [
    {
      type: 'list',
      name: 'system',
      message: 'Choose your System:',
      default: 'Drupal',
      choices: ['Drupal', 'Wordpress', 'JTL-Shop'],
    },
  ];

  return inquirer.prompt(question).then((answer) => {
    console.log(infoColor, 'System: ' + answer.system);
    if (answer.system === 'Drupal') {
      gulp
        .src('./node_modules/webks-gulp-scripts/configuration_presets/drupal/__gulpfile.variables.mjs')
        .pipe(gulp.dest('./'));
    } else if (answer.system === 'Wordpress') {
      gulp
        .src('./node_modules/webks-gulp-scripts/configuration_presets/wordpress/__gulpfile.variables.mjs')
        .pipe(gulp.dest('./'));
    } else if (answer.system === 'JTL-Shop') {
      gulp
        .src('./node_modules/webks-gulp-scripts/configuration_presets/jtl-shop/__gulpfile.variables.mjs')
        .pipe(gulp.dest('./'));
    } else {
      // Local & upload
      console.log(
        warningColor,
        'No known system choosed - just copy gulpscript.mjs - you have to create your own variables.gulpfile.mjs!',
      );
    }
    console.log(infoColor, 'Updating webks-gulp-scripts from NPM ...');
    run('npm update webks-gulp-scripts')
      .then(() => {
        console.log('Successfully updated webks-gulp-scripts');
        done();
      })
      .catch((err) => {
        console.error(err);
        done(err);
      });
    return gulp.src('./node_modules/webks-gulp-scripts/gulpfile.mjs').pipe(gulp.dest('./'), function () {
      console.log(successColor, 'Updated gulpfile.mjs from webks-gulp-scripts!');
    });
  });
});
