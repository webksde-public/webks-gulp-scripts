# webks Gulp Scripts

webks.de default scripts for Gulp frontend development. The goal of this repository is to have a central place for our current Gulp frontend scripts and be able to self-update this script from the single instances.

The gulpfile.js from this repository is regulary updated in our projects by the 'gulp self-update' task.

Our gulpfile.js comes with a gulpfile.variables.js file, with predefined variables for each of the systems we work with (Drupal, Wordpress, JTL-Shop).

## Initial Setup
1. Run "npm install webks-gulp-scripts"
2. Copy the gulpfile.mjs and YOUR_SYSTEM/__gulpfile.variables.mjs to your (themes) root directory.
3. Rename __gulpfile.variables.mjs to gulpfile.variables.mjs and modify its variables. Furthermore **you may like to add this file to your .gitignore**, if you dont like to have those informations in a public repository or whatever.

## Update gulpfile.js & __gulpfile.variables.js
1. Run 'gulp selfUpdate'

## Troubleshooting
### NPM throws "Error: Cannot find module 'x'"
run 'rm -rf node_modules && rm package-lock.json && npm install'
### BrowserSync: Firefox doesn't update the styles without clearing the cache
Simply opfen your devtools go to settings, scroll down to "Advanced settings" and check "Disable HTTP cache when toolbox is open".
Now - while your devtools are open, the changes should be visible, if BrowserReload reloads the tab.
