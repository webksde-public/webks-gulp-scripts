// BrowserReload configuration
// If inside an WSL2 enviroment, you might want to configure a prox inside your .bashrc.
// Furthermore, you might need to disable WSL's linux-gui app functionality, otherwise
// the webbrowser might launch as linux-gui-app.
export const livereload = {
  url: 'https://my.website.de',
  browser: 'firefox',
  proxyUrl: 'http://localhost:3001',
};

// The webserver configuration is used for (gulp-)rsync.
// The remote folder is syncronised with the local folder, *including deletion of remote files*.
export const webserver = {
  user: 'webX',
  host: 'my.webserver.de',
  port: 22,
  key: '~/.ssh/keys/open_ssh_rsa_X',
  remotePath: '/var/www/vhosts/my.webserver.de/httpdocs/drupal/web/themes/custom/drowl_child',
  exclsionGlobs: ['node_modules', '.*', '.*/', '.DS_Store', 'gulpfile.*'],
};

export const folderNames = {
  scss: 'scss',
  css: 'css',
  js: 'js',
  templates: 'templates',
  images: 'images',
  fonts: 'fonts',
  frontendLibraries: 'frontend_libraries',
};

export const paths = {
  scss: './' + folderNames.scss + '/',
  css: './dist/' + folderNames.css + '/',
  js: './' + folderNames.js + '/',
  jsDist: './dist/' + folderNames.js + '/',
  templates: './' + folderNames.templates + '/',
  images: './' + folderNames.images + '/',
  fonts: './' + folderNames.fonts + '/',
  frontendLibraries: './' + folderNames.frontendLibraries + '/',
};

export const globs = {
  scss: paths.scss + '**/*.scss',
  css: paths.css + '**/*.css',
  js: paths.js + '**/*.js',
  jsDist: paths.jsDist + '**/*.js',
  templates: paths.templates + '**/*.twig',
  images: paths.images + '**/*.*',
  fonts: paths.fonts + '**/*.*',
  // Misc files, like configuration files (yml), php files, ...
  misc: ['./*.yml', './*.php', './*.theme', './*.svg', './*.png'],
};

// Exclusions for the moveFrontendLibraries task (copy none dev NPM packeges into the frontent_libraries folder)
export const gulpNpmDistExcludes = [
  '*.map',
  'examples/**/*',
  'example/**/*',
  'demo/**/*',
  'spec/**/*',
  'docs/**/*',
  'tests/**/*',
  'test/**/*',
  'Gruntfile.js',
  'gulpfile.js',
  'package.json',
  'package-lock.json',
  'bower.json',
  'composer.json',
  'yarn.lock',
  'webpack.config.js',
  'README',
  'LICENSE',
  'CHANGELOG',
  '*.coffee',
  '*.ts',
];
